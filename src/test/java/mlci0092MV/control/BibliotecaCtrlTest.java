package mlci0092MV.control;

import mlci0092MV.model.Carte;
import mlci0092MV.model.repo.CartiRepo;
import mlci0092MV.repository.repoInterfaces.CartiRepoInterface;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    CartiRepoInterface cr;
    BibliotecaCtrl cs;
    Carte c;

    @Before
    public void setUp() throws Exception {
        cr = new CartiRepo();
        cs = new BibliotecaCtrl(cr);

        c = new Carte();

        String autoriA[]={"Jojo Moyes"};
        ArrayList<String> autori= new ArrayList<String>(Arrays.asList(autoriA));
        c.setAutori(autori);

        String cuv[]={"trist", "interesant"};
        ArrayList<String> cuvCh= new ArrayList<String>(Arrays.asList(cuv));
        c.setCuvinteCheie(cuvCh);

        c.setEditura("ALL");
    }

    @After
    public void tearDown() throws Exception {
        cr=null;
        cs=null;
    }

    @Test
    public void adaugaCarte1() {
        try {
            c.setTitlu("Inainte sa te cunosc");
            c.setAnAparitie("2016");

            int n = cs.getCarti().size();
            cs.adaugaCarte(c);
            assertEquals(n+1, cs.getCarti().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(expected = Exception.class)
    public void adaugaCarte2() throws Exception {
        c.setTitlu("Inainte sa te cunosc");
        c.setAnAparitie("2020");
        cs.adaugaCarte(c);
    }

    @Test(expected = Exception.class)
    public void adaugaCarte3() throws Exception {
        c.setAnAparitie("2010");
        cs.adaugaCarte(c);
    }

    @Test(expected = Exception.class)
    public void adaugaCarte4() throws Exception {
        c.setAnAparitie("2025");
        cs.adaugaCarte(c);
    }

    @Test(expected = Exception.class)
    public void adaugaCarte5() throws Exception {
        c.setAnAparitie("1900");
        cs.adaugaCarte(c);
    }

    @Test
    public void adaugaCarte6() {
        try {
            c.setTitlu("I");
            c.setAnAparitie("2016");

            int n = cs.getCarti().size();
            cs.adaugaCarte(c);
            assertEquals(n+1, cs.getCarti().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void adaugaCarte7() {
        try {
            c.setTitlu("Ceva");
            c.setAnAparitie("2018");

            int n = cs.getCarti().size();
            cs.adaugaCarte(c);
            assertEquals(n+1, cs.getCarti().size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test(expected = Exception.class)
    public void adaugaCarte8() throws Exception {
        c.setTitlu("Roman");
        c.setAnAparitie("2019");
        cs.adaugaCarte(c);
    }


}