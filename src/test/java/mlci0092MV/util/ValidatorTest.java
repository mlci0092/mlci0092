package mlci0092MV.util;

import mlci0092MV.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class ValidatorTest {


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test_isStringOK_WhenValid() throws Exception {
     boolean valid = Validator.isStringOK("Rascoala");
     assertTrue("String valid marcat ca invalid",valid);
    }

    @Test(expected = Exception.class)
    public void test_isStringOK_WhenNotValid() throws Exception {
        Validator.isStringOK("51255414554444");
    }
//se mai pott face inca 3 teste pentru a testa la maxim metoda(pt numai litere mici, numai litere mari si pt spatii)

    @Test
    public void test_validateCarte_WhenValid() throws Exception {
        Carte carte = new Carte();
        carte.setTitlu("Rascoala");
        carte.setAnAparitie("1970");
        carte.setAutori(Collections.singletonList("Liviu Rebreanu"));
        carte.setEditura("Humanitas");
        carte.setCuvinteCheie(Arrays.asList("rascoala","liviu"));
        Validator.validateCarte(carte);
    }

    @Test
    public void test_validateCarte_WhenMissingListaAutori(){
        Carte carte = new Carte();
        carte.setTitlu("Rascoala");
        carte.setAnAparitie("1970");
        carte.setAutori(null);
        carte.setEditura("Humanitas");
        carte.setCuvinteCheie(Arrays.asList("rascoala","liviu"));
        Exception exception = null;
        try {
            Validator.validateCarte(carte);
        } catch (Exception e) {
            exception = e;
        }
        assertNotNull("Nu s-a aruncat exceptia",exception);
        assertEquals("Exceptia nu a avut mesajul asteptat!",
                "Lista autori vida!",exception.getMessage());
    }
}
